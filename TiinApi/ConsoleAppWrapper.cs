using System;
using System.Diagnostics;
using System.IO;

namespace ViettelMedia.TiinApi
{
    public class ConsoleAppWrapper
    {
        public static string Execute(string exePath, string parameters)
        {
            string result = String.Empty;

            using (Process p = new Process())
            {
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.FileName = exePath;
                p.StartInfo.Arguments = parameters;
                p.Start();
                result = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
            }

            return result;
        }
    }
}