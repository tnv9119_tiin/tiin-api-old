namespace ViettelMedia.TiinApi.Commons
{
      // ReSharper disable once InconsistentNaming
      public class LibCRBT
      {
            private static string PhoneViettel(string phone)
            {
                  string[] tmp = LibConst.WsPhoneViettel.Split(',');
                  for (int i = 0; i < tmp.Length; i++)
                        if (phone.StartsWith(tmp[i]))
                              return "84" + phone;
                  return "";
            }

            private static string PhoneViettelAll(string phone)
            {
                  //ngay 22/04/2013 them dau so 162
                  string   strphone = "98,97,163,164,165,166,167,168,169,42,96,162,86";
                  string[] tmp      = strphone.Split(',');
                  for (int i = 0; i < tmp.Length; i++)
                        if (phone.StartsWith(tmp[i]))
                              return phone;
                  return "00";
            }

            public static string CheckPhoneValid(string phoneNumber)
            {
                  if (phoneNumber == "") return "";
                  string phone = "";
                  for (int i = 0; i < phoneNumber.Length; i++)
                        if (LibConst.PhoneSyntax.IndexOf(phoneNumber[i]) != -1)
                              phone += phoneNumber[i];

                  if (phone != "" && phone.Length > 3)
                  {
                        if (phone.StartsWith("84"))
                              phone                           = phone.Substring(2);
                        else if (phone.StartsWith("0")) phone = phone.Substring(1);
                  }

                  if (phone.Length != 9 && phone.Length != 10 && phone.Length != 8) return "";

                  return PhoneViettel(phone);
            }

            public static string CheckPhoneValidAll(string phoneNumber)
            {
                  if (phoneNumber == "") return "00";
                  string phone = "";
                  for (int i = 0; i < phoneNumber.Length; i++)
                        if (LibConst.PhoneSyntax.IndexOf(phoneNumber[i]) != -1)
                              phone += phoneNumber[i];

                  if (phone != "" && phone.Length > 3)
                  {
                        if (phone.StartsWith("84"))
                              phone                           = phone.Substring(2);
                        else if (phone.StartsWith("0")) phone = phone.Substring(1);
                  }

                  if (phone.Length != 9 && phone.Length != 10 && phone.Length != 8 && phone.Length != 11) return "00";

                  return PhoneViettelAll(phone);
            }

            public static string PhoneValidAll(string phoneNumber)
            {
                  //Hàm này sai
                  //Vì khi user nhập
                  if (phoneNumber == "")
                        return "";

                  string phone = "";

                  for (int i = 0; i < phoneNumber.Length; i++)
                  {
                        if (LibConst.PhoneSyntax.IndexOf(phoneNumber[i]) != -1)
                              phone += phoneNumber[i];
                  }

                  if (phone != "" && phone.Length > 3)
                  {
                        if (phone.StartsWith("84"))
                              phone = phone.Substring(2);
                        else if (phone.StartsWith("0"))
                              phone = phone.Substring(1);
                  }

                  if (phone.Length != 9 && phone.Length != 10 && phone.Length != 8 && phone.Length != 11)
                        return "";
                  else
                        return "84" + phone;
            }
      }
}