// Type: System.Runtime.CompilerServices.ExtensionAttribute
// Assembly: Newtonsoft.Json, Version=6.0.0.0, Culture=neutral, PublicKeyToken=30ad4fe6b2a6aeed
// MVID: 2CCEAD7E-60D5-4E86-87A2-3819FCE6C567
// Assembly location: (redacted)\bin\Newtonsoft.Json.dll

using System;

// ReSharper disable once CheckNamespace
namespace System.Runtime.CompilerServices
{
      ///
      /// This attribute allows us to define extension methods without
      /// requiring .NET Framework 3.5. For more information, see the section,
      /// <a href="http://msdn.microsoft.com/en-us/magazine/cc163317.aspx#S7">Extension Methods in .NET Framework 2.0 Apps</a>,
      /// of <a href="http://msdn.microsoft.com/en-us/magazine/cc163317.aspx">Basic Instincts: Extension Methods</a>
      /// column in <a href="http://msdn.microsoft.com/msdnmag/">MSDN Magazine</a>,
      /// issue <a href="http://msdn.microsoft.com/en-us/magazine/cc135410.aspx">Nov 2007</a>.
      ///
      ///
      [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method)]
      internal sealed class ExtensionAttribute : Attribute
      {
      }
}