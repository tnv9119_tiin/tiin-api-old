﻿using System;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.Others
{
      public partial class RegDevice : System.Web.UI.Page
      {
            protected void Page_Load(object sender, EventArgs e)
            {
                  string device_id           = Request.Params.Get("device_id");
                  string wifi_mac            = Request.Params.Get("wifi_mac");
                  string device_type         = Request.Params.Get("device_type");
                  string push_id             = Request.Params.Get("push_id");
                  string msisdn              = Request.Params.Get("msisdn");
                  if (msisdn == null) msisdn = "";

                  DataResponse dataResponse = new DataResponse();

                  try
                  {
                        Common.SetRegDevice(wifi_mac, push_id, device_type, msisdn, device_id);
                  }
                  catch (Exception exception)
                  {
                        dataResponse.exception = exception;
                        Response.Clear();
                        Response.ContentType = "application/json; charset=utf-8";
                        Response.StatusCode  = 500;
                        Response.Write(JsonConvert.SerializeObject("Khong thanh cong"));
                        Response.End();
                  }

                  Response.Clear();
                  Response.ContentType = "application/json; charset=utf-8";
                  Response.StatusCode  = 200;
                  Response.Write(JsonConvert.SerializeObject("Chen thanh cong"));
                  Response.End();
            }
      }
}