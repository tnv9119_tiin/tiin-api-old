﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.Others
{
      public partial class SearchDetail : System.Web.UI.Page
      {
            protected string keywords = "";
            protected string page     = "";
            protected string num      = "";

            protected void Page_Load(object sender, EventArgs e)
            {
                  keywords = Common.GetValue("keywords", "1");
                  page     = Common.GetValue("page",     "1");
                  num      = Common.GetValue("num",      "10");

                  List<Article> item1 = new List<Article>();

                  DataArticle dataArticle = new DataArticle(null);

                  keywords = HttpUtility.HtmlDecode(keywords);
                  try
                  {
                        DataSet ds = Common.searchDetail(keywords, page);

                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    Article item = new Article();
                                    item.Title           = row["title"].ToString();
                                    item.Sapo            = row["Lead"].ToString();
                                    item.Pid             = Convert.ToInt32(row["pid"].ToString());
                                    item.Cid             = Convert.ToInt32(row["cid"].ToString());
                                    item.Content         = "";
                                    item.AuthorName     = "";
                                    item.ParentCategory = row.StringValue("CategoryName");
                                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName") ? row.StringValue("ChildCategoryName") : row.StringValue("CategoryName");
                                    item.CategoryAlias = !row.IsNullOrEmptyStringValue("ChildCategoryAlias") ? row.StringValue("ChildCategoryAlias") : row.StringValue("CategoryAlias");

                                    item.Id              = Convert.ToInt32(row["ID"].ToString());
                                    if (item.Id > 884864)
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') + Path.AltDirectorySeparatorChar +
                                                       row["LeadImage"].ToString();
                                    }
                                    else
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') + Path.AltDirectorySeparatorChar +
                                                       row["LeadImage"].ToString();
                                    }
                                    item.Like     = 0;
                                    item.LoaiBai = 0;
                                    item.Position = 0;
                                    item.Reads    = 0;
                                    item.Type     = Convert.ToInt32(row["icon"]);
                                    item.StarId  = 0;
                                    item.DatePub  = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                                    item.Url      = Rewrite.GenRewriteUrlDetail(row);
                                    item1.Add(item);
                              }
                        }

                        dataArticle.data = item1;
                  }
                  catch (Exception ex)
                  {
                        dataArticle.exception = ex;
                  }

                  Response.Json(dataArticle);
            }
      }
}