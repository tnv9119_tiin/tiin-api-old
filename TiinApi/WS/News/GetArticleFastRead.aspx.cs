using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.UI;
using HtmlAgilityPack;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.News
{
      public partial class GetArticleFastRead : System.Web.UI.Page
      {
            protected string ArticleId = "0";
            protected string Phone     = "";
            protected string Msisdn    = "";

            protected string slug = "";

            protected void Page_Load(object sender, EventArgs e)
            {
                  ArticleId = Common.GetValue("article_id", "0");

                  slug = Request.QueryString["slug"] != null ? Common.ValidateXSS(Request.QueryString["slug"]) : "";


                  Article article = null;


                  try
                  {
                        DataSet ds = Common.GetArticleFastRead(ArticleId, slug);


                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    article                = new Article();
                                    article.Title          = row["Title"].ToString();
                                    article.Sapo           = row["lead"].ToString();
                                    article.Pid            = Convert.ToInt32(row["pid"].ToString());
                                    article.Cid            = Convert.ToInt32(row["cid"].ToString());
                                    article.Content        = "";
                                    article.AuthorName     = Common.GetValueOrDefault(row["Author"].ToString(),     Common.DefaultAuthorName);
                                    article.SourceName     = Common.GetValueOrDefault(row["SourceName"].ToString(), Common.DefaultSourceName);
                                    article.ParentCategory = row["CategoryName"].ToString().Trim();
                                    article.Category       = row["CategoryName"].ToString().Trim();
                                    article.Id             = Convert.ToInt32(row["ID"].ToString());
                                    if (article.Id > 884864)
                                    {
                                          article.Image = ConfigurationManager.AppSettings["MediaRootNew"] + row["LeadImage"].ToString();
                                    }
                                    else
                                    {
                                          article.Image = ConfigurationManager.AppSettings["MediaRoot"] + row["LeadImage"].ToString();
                                    }

                                    article.Like         = 0;
                                    article.LoaiBai      = Convert.ToInt32(row["icon"].ToString());
                                    article.Position     = 0;
                                    article.Reads        = Convert.ToInt32(row["hit"].ToString());
                                    article.Type         = 1;
                                    article.StarId       = 0;
                                    article.Facebook     = "";
                                    article.ThematicName = "";
                                    article.DatePub      = Convert.ToInt32(row["datePub"].ToString());
                                    article.Url          = Rewrite.GenRewriteUrlDetail(row);
                                    article.Blocks =
                                          new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()), row["id"].ToString(), out var totalBlocks);
                                    article.TotalBlocks = totalBlocks;
                              }
                        }


                        /*//Log dữ liệu
                        if ((HttpContext.Current.Session["MSISDN"] != null))
                        {
                              //lay so msisdn tu session
                              MSISDN = Session["MSISDN"].ToString();
                              MSISDN = libCRBT.checkPhoneValidALL(MSISDN);
                        }
                        else
                        {
                              MSISDN = Common.GetMSISDNOnHeader();
                              MSISDN = libCRBT.checkPhoneValidALL(MSISDN);
                              if (MSISDN.Length < 5)
                              {
                                    phone                                 = Common.getPhoneNumber(Request);
                                    MSISDN                                = libCRBT.checkPhoneValidALL(phone);
                                    HttpContext.Current.Session["MSISDN"] = MSISDN;
                              }
                              else
                              {
                                    HttpContext.Current.Session["MSISDN"] = MSISDN;
                              }
                        }

                        if (MSISDN.Length > 4)
                        {
                              Common.LogADV("0", "0", "APP", MSISDN, Request.UserAgent.ToString(), Request.RawUrl.ToString(), Request.ServerVariables["REMOTE_ADDR"].ToString());
                        }*/
                  }
                  catch (Exception ex)
                  {
                        Response.Headers.Add("Exception",  ex.Message);
                        Response.Headers.Add("StackTrace", ex.StackTrace);
                  }

                  Response.Clear();
                  Response.ContentType = "application/json; charset=utf-8";
                  Response.Write(JsonConvert.SerializeObject(article));
                  Response.End();
            }
      }
}