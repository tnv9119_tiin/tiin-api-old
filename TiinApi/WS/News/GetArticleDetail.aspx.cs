﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.News
{
      public partial class GetArticleDetail : System.Web.UI.Page
      {
            protected string article_id = "0";
            protected string phone      = "";
            protected string MSISDN     = "";

            protected void Page_Load(object sender, EventArgs e)
            {
                  article_id = Common.GetValue("article_id", "1");

                  List<Article> item1 = new List<Article>();

                  DataArticle dataArticle = new DataArticle(null);

                  try
                  {
                        DataSet ds = Common.getArticleDetail(article_id);


                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    Article item = new Article();
                                    item.Title   = row["Title"].ToString();
                                    item.Sapo    = row["lead"].ToString();
                                    item.Pid     = Convert.ToInt32(row["pid"].ToString());
                                    item.Cid     = Convert.ToInt32(row["cid"].ToString());
                                    item.Content = Common.GenLinkImageFull(row["Content"].ToString());
                                    item.ReplaceImageCaption();
                                    item.AuthorName     = row["Author"].ToString() + " / " + row["SourceName"].ToString();
                                    item.SourceName     = row["SourceName"].ToString();
                                    item.ParentCategory = row.StringValue("CategoryName");

                                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");

                                    item.Category       = !row.IsNullOrEmptyStringValue("ChildCategoryName") ? row.StringValue("ChildCategoryName") : row.StringValue("CategoryName");
                                    item.CategoryAlias = !row.IsNullOrEmptyStringValue("ChildCategoryAlias") ? row.StringValue("ChildCategoryAlias") : row.StringValue("CategoryAlias");

                                    item.Id = Convert.ToInt32(row["ID"].ToString());
                                    if (item.Id > 884864)
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') + Path.AltDirectorySeparatorChar + row["LeadImage"].ToString();
                                    }
                                    else
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') + Path.AltDirectorySeparatorChar + row["LeadImage"].ToString();
                                    }

                                    item.Like          = 0;
                                    item.LoaiBai      = Convert.ToInt32(row["icon"].ToString());
                                    item.Position      = 0;
                                    item.Reads         = Convert.ToInt32(row["hit"].ToString());
                                    item.Type          = 1;
                                    item.StarId       = 0;
                                    item.Facebook      = "";
                                    item.ThematicName = "";
                                    item.DatePub       = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                                    item.Url           = Rewrite.GenRewriteUrlDetail(row);
                                    item1.Add(item);
                              }

                              foreach (DataRow row in ds.Tables[1].Rows)
                              {
                                    Article item = new Article();
                                    item.Title           = row["Title"].ToString();
                                    item.Sapo            = row["lead"].ToString();
                                    item.Pid             = Convert.ToInt32(row["pid"].ToString());
                                    item.Cid             = Convert.ToInt32(row["cid"].ToString());
                                    item.Content         = "";
                                    item.AuthorName     = "";
                                    item.SourceName     = "";
                                    item.ParentCategory = row["CategoryName"].ToString();
                                    item.Category        = row["CategoryName"].ToString();
                                    item.Id              = Convert.ToInt32(row["ID"].ToString());
                                    if (item.Id > 884864)
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') + Path.AltDirectorySeparatorChar + row["LeadImage"].ToString();
                                    }
                                    else
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') + Path.AltDirectorySeparatorChar + row["LeadImage"].ToString();
                                    }

                                    item.Like          = 0;
                                    item.LoaiBai      = Convert.ToInt32(row["icon"].ToString());
                                    item.Position      = 0;
                                    item.Reads         = Convert.ToInt32(row["hit"].ToString());
                                    item.Type          = 1;
                                    item.StarId       = 0;
                                    item.Facebook      = "";
                                    item.ThematicName = "";
                                    item.DatePub       = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                                    item.Url           = Rewrite.GenRewriteUrlDetail(row);
                                    item1.Add(item);
                              }
                        }

                        //Log dữ liệu
                        if ((HttpContext.Current.Session["MSISDN"] != null))
                        {
                              //lay so msisdn tu session
                              MSISDN = Session["MSISDN"].ToString();
                              MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                        }
                        else
                        {
                              MSISDN = Common.GetMSISDNOnHeader();
                              MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                              if (MSISDN.Length < 5)
                              {
                                    phone                                 = Common.getPhoneNumber(Request);
                                    MSISDN                                = LibCRBT.CheckPhoneValidAll(phone);
                                    HttpContext.Current.Session["MSISDN"] = MSISDN;
                              }
                              else
                              {
                                    HttpContext.Current.Session["MSISDN"] = MSISDN;
                              }
                        }

                        if (MSISDN.Length > 4)
                        {
                              Common.LogADV("0", "0", "APP", MSISDN, Request.UserAgent.ToString(), Request.RawUrl.ToString(), Request.ServerVariables["REMOTE_ADDR"].ToString());
                        }
                  }
                  catch (Exception ex)
                  {
                        dataArticle.exception = ex;
                  }

                  dataArticle.data = item1;
                  Response.Json(dataArticle);
            }
      }
}