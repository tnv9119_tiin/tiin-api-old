﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.News
{
      public partial class GetListStarByType : System.Web.UI.Page
      {
            protected string page    = "1";
            protected string num     = "10";
            protected string type_id = "0";
            protected string phone   = "";
            protected string MSISDN  = "";


            protected void Page_Load(object sender, EventArgs e)
            {
                  page    = Common.GetValue("page",    "1");
                  num     = Common.GetValue("num",     "10");
                  type_id = Common.GetValue("type_id", "0");

                  List<Article> item1 = new List<Article>();

                  DataArticle dataArticle = new DataArticle(null);

                  try
                  {
                        DataSet ds = Common.getListStarByType(type_id, page, num);


                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    Article item = new Article();
                                    item.Title           = row["fullname"].ToString();
                                    item.Sapo            = "";
                                    item.Pid             = 0;
                                    item.Cid             = 0;
                                    item.Content         = "";
                                    item.AuthorName     = "";
                                    item.ParentCategory = row.StringValue("CategoryName");
                                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName") ? row.StringValue("ChildCategoryName") : row.StringValue("CategoryName");
                                    item.CategoryAlias = !row.IsNullOrEmptyStringValue("ChildCategoryAlias") ? row.StringValue("ChildCategoryAlias") : row.StringValue("CategoryAlias");

                                    item.Id              = Convert.ToInt32(row["ID"].ToString());
                                    if (item.Id > 884864)
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/')+  Path.AltDirectorySeparatorChar +
                                                       row["avatar_path"].ToString();
                                    }
                                    else
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/')+ Path.AltDirectorySeparatorChar +
                                                       row["avatar_path"].ToString();
                                    }

                                    item.Like          = 0;
                                    item.LoaiBai      = 0;
                                    item.Position      = 0;
                                    item.Reads         = 0;
                                    item.Type          = 1;
                                    item.StarId       = 0;
                                    item.Facebook      = "";
                                    item.ThematicName = "";
                                    item.DatePub       = 0;
                                    item.FanCount     = Convert.ToInt32(row["fan_count"].ToString());
                                    item.VideoId      = Convert.ToInt32(row["video_id"].ToString());
                                    item.Url           = "";
                                    item1.Add(item);
                              }

                              dataArticle.data = item1;

                              //Log dữ liệu
                              if ((HttpContext.Current.Session["MSISDN"] != null))
                              {
                                    //lay so msisdn tu session
                                    MSISDN = Session["MSISDN"].ToString();
                                    MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                              }
                              else
                              {
                                    MSISDN = Common.GetMSISDNOnHeader();
                                    MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                                    if (MSISDN.Length < 5)
                                    {
                                          phone                                 = Common.getPhoneNumber(Request);
                                          MSISDN                                = LibCRBT.CheckPhoneValidAll(phone);
                                          HttpContext.Current.Session["MSISDN"] = MSISDN;
                                    }
                                    else
                                    {
                                          HttpContext.Current.Session["MSISDN"] = MSISDN;
                                    }
                              }

                              if (MSISDN.Length > 4)
                              {
                                    Common.LogADV("0", "0", "APP", MSISDN, Request.UserAgent.ToString(),
                                                  Request.RawUrl.ToString(),
                                                  Request.ServerVariables["REMOTE_ADDR"].ToString());
                              }
                        }
                  }
                  catch (Exception ex)
                  {
                        dataArticle.exception = ex;
                  }

                  Response.Json(dataArticle);
            }
      }
}