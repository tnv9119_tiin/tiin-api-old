﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.News
{
      public partial class GetListCategory : System.Web.UI.Page
      {
            protected string phone  = "";
            protected string MSISDN = "";

            protected void Page_Load(object sender, EventArgs e)
            {
                  List<Category> item1 = new List<Category>();

                  DataCategory dataCategory = new DataCategory(null);

                  try
                  {
                        DataSet ds = Common.getListCategory();


                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    Category item = new Category();
                                    item.Id   = Convert.ToInt32(row["id"].ToString());
                                    item.Name = row["name"].ToString();

                                    item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/')+ Path.AltDirectorySeparatorChar +
                                                 row["image"].ToString();

                                    item1.Add(item);
                              }

                              dataCategory.data = item1;

                              //Log dữ liệu
                              if ((HttpContext.Current.Session["MSISDN"] != null))
                              {
                                    //lay so msisdn tu session
                                    MSISDN = Session["MSISDN"].ToString();
                                    MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                              }
                              else
                              {
                                    MSISDN = Common.GetMSISDNOnHeader();
                                    MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                                    if (MSISDN.Length < 5)
                                    {
                                          phone                                 = Common.getPhoneNumber(Request);
                                          MSISDN                                = LibCRBT.CheckPhoneValidAll(phone);
                                          HttpContext.Current.Session["MSISDN"] = MSISDN;
                                    }
                                    else
                                    {
                                          HttpContext.Current.Session["MSISDN"] = MSISDN;
                                    }
                              }

                              if (MSISDN.Length > 4)
                              {
                                    Common.LogADV("0", "0", "APP", MSISDN, Request.UserAgent.ToString(),
                                                  Request.RawUrl.ToString(),
                                                  Request.ServerVariables["REMOTE_ADDR"].ToString());
                              }
                        }
                  }
                  catch (Exception ex)
                  {
                        dataCategory.exception = ex;
                        Response.Json(dataCategory);

                  }

                  Response.Json(dataCategory);
            }
      }
}