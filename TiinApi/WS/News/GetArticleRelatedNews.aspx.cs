using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.News
{
      public class GetArticleRelatedNews : Page
      {
            protected string ArticleId   = "0";
            protected string Phone       = "";
            protected string Msisdn      = "";
            protected string UrlMediaOld = ConfigurationManager.AppSettings["MediaRoot"];
            protected string UrlMediaNew = ConfigurationManager.AppSettings["MediaRootNew"];

            protected void Page_Load(object sender, EventArgs e)
            {
                  ArticleId = Common.GetValue("article_id", "1");

                  List<Article> listArticles = new List<Article>();

                  DataArticle dataArticle = null;

                  Response.Clear();
                  Response.ContentType = "application/json; charset=utf-8";

                  try
                  {
                        DataSet ds = Common.getArticleRelatedNews(ArticleId);


                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    Article item = new Article();
                                    item.Title           = row["Title"].ToString();
                                    item.Sapo            = row["lead"].ToString();
                                    item.Pid             = Convert.ToInt32(row["pid"].ToString());
                                    item.Cid             = Convert.ToInt32(row["cid"].ToString());
                                    item.Content         = "";
                                    item.AuthorName     = row["Author"].ToString();
                                    item.ParentCategory = row["CategoryName"].ToString();
                                    item.Category = row["CategoryName"].ToString();
                                    item.Id              = Convert.ToInt32(row["ID"].ToString());
                                    if (item.Id > 884864)
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRootNew"] + row["LeadImage"].ToString();
                                    }
                                    else
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRoot"] + row["LeadImage"].ToString();
                                    }

                                    item.Like          = 0;
                                    item.LoaiBai      = Convert.ToInt32(row["icon"].ToString());
                                    item.Position      = 0;
                                    item.Reads         = Convert.ToInt32(row["hit"].ToString());
                                    item.Type          = 1;
                                    item.StarId       = 0;
                                    item.Facebook      = "";
                                    item.ThematicName = "";
                                    item.DatePub       = Convert.ToInt32(row["datePub"].ToString());
                                    item.Url           = Rewrite.GenRewriteUrlDetail(row);

                                    listArticles.Add(item);
                              }
                        }


                        //Log dữ liệu
                        if ((HttpContext.Current.Session["MSISDN"] != null))
                        {
                              //lay so msisdn tu session
                              Msisdn = Session["MSISDN"].ToString();
                              Msisdn = LibCRBT.CheckPhoneValidAll(Msisdn);
                        }
                        else
                        {
                              Msisdn = Common.GetMSISDNOnHeader();
                              Msisdn = LibCRBT.CheckPhoneValidAll(Msisdn);
                              if (Msisdn.Length < 5)
                              {
                                    Phone                                 = Common.getPhoneNumber(Request);
                                    Msisdn                                = LibCRBT.CheckPhoneValidAll(Phone);
                                    HttpContext.Current.Session["MSISDN"] = Msisdn;
                              }
                              else
                              {
                                    HttpContext.Current.Session["MSISDN"] = Msisdn;
                              }
                        }

                        if (Msisdn.Length > 4)
                        {
                              Common.LogADV("0", "0", "APP", Msisdn, Request.UserAgent.ToString(), Request.RawUrl.ToString(), Request.ServerVariables["REMOTE_ADDR"].ToString());
                        }
                  }
                  catch (Exception ex)
                  {
                        listArticles = null;
                        Response.Headers.Add("Exception",  ex.Message);
                        Response.Headers.Add("StackTrace", ex.StackTrace);
                  }


                  dataArticle = new DataArticle(listArticles);
                  string json = JsonConvert.SerializeObject(dataArticle);
                  Response.Write(json);
                  Response.End();
            }
      }
}