﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;
using tiin.mocha.api.TiinApi.Mocha.Models;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Mocha.Models;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.Mocha
{
    public class ListVideo : System.Web.UI.Page
    {
        protected string page = "1";
        protected string num = "10";
        protected string category_id = "0";
        protected string phone = "";
        protected string MSISDN = "";
        protected bool isVip = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            page = Common.GetValue("page", "1");
            num = Common.GetValue("num", "10");
            category_id = Common.GetValue("id", "0");
            isVip = Common.GetValue("isVip", "0") == "1";

            DataArticle dataArticle = new DataArticle(null);

            var mochaArticles = KPILogger.LogKPI("ListVideo", () =>
            {
                var _mochaArticles = new List<MochaVideo>();


                try
                {
                    DataSet ds = Common.getArticleOfCategoryHaveContent("94", page, num);


                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            var video = new MochaVideo();

                            video.Id = Convert.ToInt32(row["ID"].ToString());
                            video.Pid = Convert.ToInt32(row["pid"].ToString());
                            video.Cid = Convert.ToInt32(row["cid"].ToString());
                            video.Title = row["Title"].ToString();
                            video.Shapo = row["lead"].ToString();
                            string MediaUrl = video.Id > 884864
                                ? isVip ? ConfigurationManager.AppSettings["MediaRootVipNew"].TrimEnd('/') :
                                ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/')
                                : isVip
                                    ? ConfigurationManager.AppSettings["MediaRootVip"].TrimEnd('/')
                                    : ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/');

                            video.Image = MediaUrl + Path.AltDirectorySeparatorChar + row["LeadImage"].ToString();

                            video.Url = Rewrite.GenRewriteUrlDetail(row);
                            video.Media = MediaUrl +
                                          Path.AltDirectorySeparatorChar + row["Media"].ToString();

                            video.Poster = "";
                            video.SourceName = row.Field<string>("SourceName");
                            video.SourceIcon = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                               "/archive/flash/2019/06/27/flash012354292069758.png";
                            video.DatePub = "<span style=\"color:#6D6D6D; font-size: 12px;\">" +
                                            row.Field<string>("DatePub") + "</span>";
                            video.View = row.Field<int>("Hit");
                            video.UnixTime = row.Field<long>("UnixTime");
                            video.Sid = int.Parse(row["Sid"].ToString());

                            var downloadAbleMedia = ConfigurationManager.AppSettings["MediaRootDownload"].TrimEnd('/') +
                                                    Path.AltDirectorySeparatorChar + row["Media"].ToString();

                            int height = 9, width = 16;
                            decimal ratio = (decimal) width / height;
                            

//                            var ffprobeReturn = CacheManager.Remember(downloadAbleMedia, DateTime.Now.AddMonths(1),
//                                delegate
//                                {
//                                    return ConsoleAppWrapper.Execute(Server.MapPath("~\\App_Data\\ffmpeg\\ffprobe.exe"),
//                                        " -hide_banner -show_format -show_streams -pretty " + "\"" + downloadAbleMedia +
//                                        "\"");
//                                });
//
//                            long duration;
//
//                            try
//                            {
//                                var widthStr = Regex
//                                    .Match(ffprobeReturn, @"^width=(?<width>\d+)", RegexOptions.Multiline)
//                                    .Groups["width"].Value;
//                                var heightStr = Regex
//                                    .Match(ffprobeReturn, @"^height=(?<height>\d+)", RegexOptions.Multiline)
//                                    .Groups["height"].Value;
//                                var durationStr = Regex.Match(ffprobeReturn, @"^duration_ts=(?<duration_ts>\d+)",
//                                    RegexOptions.Multiline).Groups["duration_ts"].Value;
//                                height = int.Parse(heightStr.Trim());
//                                width = int.Parse(widthStr.Trim());
//                                duration = long.Parse(durationStr.Trim());
//                                video.inf = width + ":" + height + "@" + duration;
//                                video.duration = duration;
//                            }
//                            catch (Exception ex)
//                            {
//                                // Syntax error in the regular expression
//                            }

                            decimal dbRatio = ratio;
                            decimal.TryParse(row["Ratio"].ToString(), out dbRatio);
                            if (dbRatio != 0)
                            {
                                ratio = dbRatio;
                            }

                            video.aspecRatio = ratio.ToString("0.000");
                            
                            _mochaArticles.Add(video);
                        }
                    }
                }
                catch (Exception ex)
                {
                    dataArticle.exception = ex;
                }

                return _mochaArticles;
            });

            dataArticle.data = mochaArticles;
            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.StatusCode = 200;
            Response.Write(JsonConvert.SerializeObject(new DataResponse {data = mochaArticles},
                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Include}));
            Response.End();
        }
    }
}