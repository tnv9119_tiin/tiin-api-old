﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Mocha.Models;
using ViettelMedia.TiinApi.Mocha.Models.Response;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;
using ViettelMedia.TiinApi.WS.News;

namespace ViettelMedia.TiinApi.Mocha
{
    public partial class LatestNews : System.Web.UI.Page
    {
        protected string phone = "";
        protected string MSISDN = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            var pid = Common.GetValue("pid", "0");
            var cid = Common.GetValue("cid", "0");
            var page = Common.GetValue("page", "1");
            var num = Common.GetValue("num", "10");

            var responseJson = "";
            try
            {
                responseJson = KPILogger.LogKPI("LatestNews", () =>
                {
                    var cacheName =
                        String.Join("_",
                            new[] {"Tiin_API$Mocha$LatestNewsSingle", pid, cid, page.ToString(), num.ToString()});

                    return CacheManager.Remember<string>(cacheName,
                        DateTime.Now.AddSeconds(Common.SecondsCache),
                        delegate
                        {
                            IEnumerable<object> _latestNews = new List<object>();
                            DataSet ds = Common.Tiin_API_Mocha_LatestNews(pid, cid, page, num);


                            if (ds.Tables.Count > 0)
                            {
                                _latestNews = DataTableToListArticle(ds.Tables[0]);
                            }

                            return JsonConvert.SerializeObject(new MochaHomeResponse(_latestNews),
                                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Include});
                        });
                });
            }
            catch (Exception exception)
            {
                responseJson = JsonConvert.SerializeObject(new MochaHomeResponse(null) {exception = exception},
                    new JsonSerializerSettings {NullValueHandling = NullValueHandling.Include});
            }

            Response.JsonString(responseJson);
        }


        protected object DataRowToArticle(DataRow row)
        {
            var item = new MochaArticle();

            item.Title = row["Title"].ToString();
            item.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
            item.Sapo = row["lead"].ToString();
            item.Pid = Convert.ToInt32(row["pid"].ToString());
            item.Cid = Convert.ToInt32(row["cid"].ToString());
            item.Content = "";
            item.AuthorName = "";
            item.ParentCategory = row.StringValue("CategoryName");
            item.ParentCategoryAlias = row.StringValue("CategoryAlias");
            item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                ? row.StringValue("ChildCategoryName")
                : row.StringValue("CategoryName");
            item.CategoryAlias =
                !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                    ? row.StringValue("ChildCategoryAlias")
                    : row.StringValue("CategoryAlias");
            item.Header = "";
            item.Id = Convert.ToInt32(row["ID"].ToString());
            item.Id = Convert.ToInt32(row["ID"].ToString());
            if (item.Id > 884864)
            {
                item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                             Path.AltDirectorySeparatorChar +
                             row["LeadImage"].ToString();

                if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                {
                    item.Image169 = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                    Path.AltDirectorySeparatorChar +
                                    row["LeadImage420"].ToString();
                }
            }
            else
            {
                item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                             Path.AltDirectorySeparatorChar +
                             row["LeadImage"].ToString();
                if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                {
                    item.Image169 = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                    Path.AltDirectorySeparatorChar +
                                    row["LeadImage420"].ToString();
                }
            }

            item.Like = 0;
            item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
            item.Position = 0;
            item.Reads = Convert.ToInt32(row["hit"].ToString());
            item.Type = 1;
            item.StarId = 0;
            item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
            item.Url = Rewrite.GenRewriteUrlDetail(row);

            item.Blocks = new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()),
                    row["id"].ToString(), out var totalBlocks, 3)
                .ConvertAll(MochaArticleBlock.Converter);

            item.TotalBlocks = totalBlocks;
            return item;
        }


        protected List<MochaArticle> DataTableToListArticle(DataTable dataTable, string Header = "")
        {
            var articles = new List<MochaArticle>();
            try

            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var item = new MochaArticle();

                    item.Title = row["Title"].ToString();
                    item.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                    item.Sapo = row["lead"].ToString();
                    item.Pid = Convert.ToInt32(row["pid"].ToString());
                    item.Cid = Convert.ToInt32(row["cid"].ToString());
                    item.Content = "";
                    item.AuthorName = "";
                    item.ParentCategory = row.StringValue("CategoryName");
                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                        ? row.StringValue("ChildCategoryName")
                        : row.StringValue("CategoryName");
                    item.CategoryAlias =
                        !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                            ? row.StringValue("ChildCategoryAlias")
                            : row.StringValue("CategoryAlias");
                    item.Header = Header;
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    if (item.Id > 884864)
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();

                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }
                    else
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }

                    item.Like = 0;
                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                    item.Position = 0;
                    item.Reads = Convert.ToInt32(row["hit"].ToString());
                    item.Type = 1;
                    item.StarId = 0;
                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                    item.Url = Rewrite.GenRewriteUrlDetail(row);

                    item.Blocks = new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()),
                            row["id"].ToString(), out var totalBlocks, 3)
                        .ConvertAll(MochaArticleBlock.Converter);

                    item.TotalBlocks = totalBlocks;
                    articles.Add(item);
                }
            }
            catch (Exception e)
            {
//                Console.WriteLine(e);
                throw;
                return null;
            }

            return articles;
        }

        protected List<MochaArticle> DataTableToListThematic(DataTable dataTable, string Header = "Ngay lúc này")
        {
            var articles = new List<MochaArticle>();
            try

            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var item = new MochaArticle();
                    item.LatestTitle = row["Title"].ToString();
                    item.Title = row["thematic_name"].ToString();
                    item.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                    item.Sapo = row["lead"].ToString();
                    item.Pid = Convert.ToInt32(row["pid"].ToString());
                    item.Cid = Convert.ToInt32(row["cid"].ToString());
                    item.Content = "";
                    item.AuthorName = "";
                    item.ParentCategory = row.StringValue("CategoryName");
                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                        ? row.StringValue("ChildCategoryName")
                        : row.StringValue("CategoryName");
                    item.CategoryAlias =
                        !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                            ? row.StringValue("ChildCategoryAlias")
                            : row.StringValue("CategoryAlias");
                    item.Header = Header;
                    item.Id = Convert.ToInt32(row["thematic_id"].ToString());
                    var postId = Convert.ToInt32(row["Id"].ToString());
                    if (item.Id > 884864)
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();

                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }
                    else
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }

                    item.Like = 0;
                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                    item.Position = 0;
                    item.Reads = Convert.ToInt32(row["hit"].ToString());
                    item.Type = 1;
                    item.StarId = 0;
                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                    item.Url = Rewrite.GenRewriteUrlDetail(row);

                    item.Blocks = new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()),
                            row["id"].ToString(), out var totalBlocks, 3)
                        .ConvertAll(MochaArticleBlock.Converter);

                    item.TotalBlocks = totalBlocks;
                    articles.Add(item);
                }
            }
            catch (Exception e)
            {
//                Console.WriteLine(e);
//                throw;
                return null;
            }

            return articles;
        }

        protected List<MochaArticle> dataTableToListQuotes(DataTable dataTable)
        {
            var articles = new List<MochaArticle>();
            try

            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var item = new MochaArticle();
                    item.Quote = row["Quote"].ToString();
                    item.Poster = row["Poster"].ToString();
                    item.Title = row["Title"].ToString();
                    item.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                    item.Sapo = row["lead"].ToString();
                    item.Pid = Convert.ToInt32(row["pid"].ToString());
                    item.Cid = Convert.ToInt32(row["cid"].ToString());
                    item.Content = "";
                    item.AuthorName = "";
                    item.ParentCategory = row.StringValue("CategoryName");
                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                        ? row.StringValue("ChildCategoryName")
                        : row.StringValue("CategoryName");
                    item.CategoryAlias =
                        !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                            ? row.StringValue("ChildCategoryAlias")
                            : row.StringValue("CategoryAlias");
                    item.Header = "Câu trích dẫn";
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    if (item.Id > 884864)
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                    }
                    else
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                    }

                    item.Like = 0;
                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                    item.Position = 0;
                    item.Reads = Convert.ToInt32(row["hit"].ToString());
                    item.Type = 1;
                    item.StarId = 0;
                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                    item.Url = Rewrite.GenRewriteUrlDetail(row);

                    item.Blocks = new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()),
                            row["id"].ToString(), out var totalBlocks, 3)
                        .ConvertAll(MochaArticleBlock.Converter);

                    item.TotalBlocks = totalBlocks;
                    articles.Add(item);
                }
            }
            catch (Exception e)
            {
//                Console.WriteLine(e);
//                throw;
                return null;
            }

            return articles;
        }
    }
}