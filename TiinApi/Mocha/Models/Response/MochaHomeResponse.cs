using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.Mocha.Models.Response
{
    public class MochaHomeResponse : DataResponse
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="homeMochaSections"></param>
        public MochaHomeResponse(IEnumerable<object> homeMochaSections)
        {
            data = homeMochaSections.ToList();
        }
    }
}