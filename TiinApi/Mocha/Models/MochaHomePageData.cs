using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Models;

namespace ViettelMedia.TiinApi.Mocha.Models
{
    public class MochaHomePageData2
    {
        public MochaHomePageData2()
        {
            Top = new List<Article>();
            
            Thematic = new List<Thematic>();
            
            Hot = new List<Article>();
            
            MostView = new List<Article>();
            
            Quote = new List<Quote>();
            
            Cates = new List<Cate>();
        }

        public List<Article> Top { get; set; }
        public List<Thematic> Thematic { get; set; }
        public List<Article> Hot { get; set; }
        public List<Article> MostView { get; set; }
        public List<Quote> Quote { get; set; }
        public List<Cate> Cates { get; set; }
        
        [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public Exception Exeption { get; set; }
    }
}