﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Mocha.Models;
using ViettelMedia.TiinApi.Mocha.Models.Response;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;
using ViettelMedia.TiinApi.WS.News;

namespace ViettelMedia.TiinApi.Mocha
{
    public partial class Homepage : System.Web.UI.Page
    {
        protected string phone = "";
        protected string MSISDN = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            var setting = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Include
            };

            string jsonData;
            try
            {
                jsonData = KPILogger.LogKPI("HomePage", () =>
                {
                    return CacheManager.Remember("Tiin_API_Mocha_HomePage_Single",
                        DateTime.Now.AddSeconds(Common.SecondsCache),
                        delegate
                        {
                            DataSet ds = Common.Tiin_API_Mocha_HomePage();
                            var sections = new List<object>();

                            if (ds.Tables.Count > 0)
                            {
                                sections.Add(new MochaHomeSection
                                {
                                    Type = "Home",
                                    Position = 0,
                                    Header = "Tin Top",
                                    data = DataTableToListArticle(ds.Tables[0], "Tin Top")
                                });

                                sections.Add(new MochaHomeSection
                                {
                                    Type = "Home",
                                    Position = 1,
                                    Header = "Sự kiện đang nóng",
                                    data = DataTableToListThematic(ds.Tables[1])
                                });

                                sections.Add(new MochaHomeSection
                                {
                                    Type = "Home",
                                    Position = 2,
                                    Header = "Tiêu điểm",
                                    data = DataTableToListArticle(ds.Tables[2], "Tiêu điểm")
                                });

                                sections.Add(new MochaHomeSection
                                {
                                    Type = "Home",
                                    Position = 3,
                                    Header = "Đọc nhiều",
                                    data = DataTableToListArticle(ds.Tables[3], "Đọc nhiều")
                                });
                                sections.Add(new MochaHomeSection
                                {
                                    Type = "Home",
                                    Position = 4,
                                    Header = "Câu trích dẫn",
                                    data = dataTableToListQuotes(ds.Tables[4])
                                });

                                //                    _homeData.Quote = dataTableToListQuotes(ds.Tables[4]);
                            }

                            var catePos = 5; // Continue block of before
                            var cateIds = (ConfigurationManager.AppSettings["MochaHomeCates"] + "").Split(',');
                            var cateDisplayTypes =
                                (ConfigurationManager.AppSettings["MochaHomeCatesDisplay"] + "").Split(',');
                            var cates = new List<Cate>();
                            foreach (var cateId in cateIds.Where(x => !string.IsNullOrEmpty(x)))
                            {
                                var dataSet = Common.Tiin_API_Mocha_HomePage_Cate(cateId);

                                if (dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                                {
                                    sections.Add(new
                                    {
                                        Type = "Category",
                                        CategoryID = int.Parse(dataSet.Tables[0].Rows[0]["Id"].ToString()),
                                        CategoryName = dataSet.Tables[0].Rows[0]["Name"].ToString(),
                                        TypeDisplay =
                                            int.Parse(cateDisplayTypes.ElementAtOrDefault(
                                                          Array.IndexOf(cateIds, cateId)) ??
                                                      "1"),
                                        data = DataTableToListArticle(dataSet.Tables[1],
                                            dataSet.Tables[0].Rows[0]["Name"].ToString()),
                                        Position = catePos++,
                                        Header = dataSet.Tables[0].Rows[0]["Name"].ToString(),
                                    });
                                }
                            }

                            return JsonConvert.SerializeObject(new MochaHomeResponse(sections), setting);
                        });
                });
            }
            catch (Exception exception)
            {
                jsonData = JsonConvert.SerializeObject(new MochaHomeResponse(new List<object>())
                {
                    exception = exception
                }, setting);
            }

            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.StatusCode = 200;
            Response.Write(jsonData);
            Response.End();
        }

        protected List<MochaArticle> DataTableToListArticle(DataTable dataTable, string Header = "")
        {
            var articles = new List<MochaArticle>();
            try

            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var item = new MochaArticle();
                    item.Title = row["Title"].ToString();
                    item.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                    item.Sapo = row["lead"].ToString();
                    item.Pid = Convert.ToInt32(row["pid"].ToString());
                    item.Cid = Convert.ToInt32(row["cid"].ToString());
                    item.Content = "";
                    item.AuthorName = "";
                    item.ParentCategory = row.StringValue("CategoryName");
                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                        ? row.StringValue("ChildCategoryName")
                        : row.StringValue("CategoryName");
                    item.CategoryAlias = !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                        ? row.StringValue("ChildCategoryAlias")
                        : row.StringValue("CategoryAlias");
                    item.Header = Header;
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    if (item.Id > 884864)
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();

                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }
                    else
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }

                    item.Like = 0;
                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                    item.Position = 0;
                    item.Reads = Convert.ToInt32(row["hit"].ToString());
                    item.Type = 1;
                    item.StarId = 0;
                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                    item.Url = Rewrite.GenRewriteUrlDetail(row);

                    item.Blocks = new FastRead().GetContentBlocks(
                            Common.GenLinkImageFull(row["Content"].ToString()),
                            row["id"].ToString(), out var totalBlocks, 3)
                        .ConvertAll(MochaArticleBlock.Converter);

                    item.TotalBlocks = totalBlocks;
                    articles.Add(item);
                }
            }
            catch (Exception e)
            {
                //                Console.WriteLine(e);
                throw;
                return null;
            }

            return articles;
        }

        protected List<MochaArticle> DataTableToListThematic(DataTable dataTable, string Header = "Ngay lúc này")
        {
            var articles = new List<MochaArticle>();
            try

            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var item = new MochaArticle();
                    item.LatestTitle = row["Title"].ToString();
                    item.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                    item.Title = row["thematic_name"].ToString();
                    item.Sapo = row["lead"].ToString();
                    item.Pid = Convert.ToInt32(row["pid"].ToString());
                    item.Cid = Convert.ToInt32(row["cid"].ToString());
                    item.Content = "";
                    item.AuthorName = "";
                    item.ParentCategory = row.StringValue("CategoryName");
                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                        ? row.StringValue("ChildCategoryName")
                        : row.StringValue("CategoryName");
                    item.CategoryAlias = !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                        ? row.StringValue("ChildCategoryAlias")
                        : row.StringValue("CategoryAlias");
                    item.Header = Header;
                    item.Id = Convert.ToInt32(row["thematic_id"].ToString());
                    var postId = Convert.ToInt32(row["Id"].ToString());
                    if (postId > 884864)
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();

                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }
                    else
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }

                    item.Like = 0;
                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                    item.Position = 0;
                    item.Reads = Convert.ToInt32(row["hit"].ToString());
                    item.Type = 1;
                    item.StarId = 0;
                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                    item.Url = Rewrite.GenRewriteUrlDetail(row);

                    item.Blocks = new FastRead().GetContentBlocks(
                            Common.GenLinkImageFull(row["Content"].ToString()),
                            row["id"].ToString(), out var totalBlocks, 3)
                        .ConvertAll(MochaArticleBlock.Converter);

                    item.TotalBlocks = totalBlocks;
                    articles.Add(item);
                }
            }
            catch (Exception e)
            {
                //                Console.WriteLine(e);
                //                throw;
                return null;
            }

            return articles;
        }

        protected List<MochaArticle> dataTableToListQuotes(DataTable dataTable)
        {
            var articles = new List<MochaArticle>();
            try

            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var item = new MochaArticle();
                    item.Quote = row["Quote"].ToString();
                    item.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                    item.Poster = row["Poster"].ToString();
                    item.Title = row["Title"].ToString();
                    item.Sapo = row["lead"].ToString();
                    item.Pid = Convert.ToInt32(row["pid"].ToString());
                    item.Cid = Convert.ToInt32(row["cid"].ToString());
                    item.Content = "";
                    item.AuthorName = "";
                    item.ParentCategory = row.StringValue("CategoryName");
                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                        ? row.StringValue("ChildCategoryName")
                        : row.StringValue("CategoryName");
                    item.CategoryAlias = !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                        ? row.StringValue("ChildCategoryAlias")
                        : row.StringValue("CategoryAlias");
                    item.Header = "Câu trích dẫn";
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    if (item.Id > 884864)
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();

                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }
                    else
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }

                    item.Like = 0;
                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                    item.Position = 0;
                    item.Reads = Convert.ToInt32(row["hit"].ToString());
                    item.Type = 1;
                    item.StarId = 0;
                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                    item.Url = Rewrite.GenRewriteUrlDetail(row);

                    item.Blocks = new FastRead().GetContentBlocks(
                            Common.GenLinkImageFull(row["Content"].ToString()),
                            row["id"].ToString(), out var totalBlocks, 3)
                        .ConvertAll(MochaArticleBlock.Converter);

                    item.TotalBlocks = totalBlocks;
                    articles.Add(item);
                }
            }
            catch (Exception e)
            {
                //                Console.WriteLine(e);
                //                throw;
                return null;
            }

            return articles;
        }
    }
}