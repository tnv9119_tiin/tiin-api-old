﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Mocha.Models;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.Mocha
{
    public partial class Article : System.Web.UI.Page
    {
        protected string article_id = "0";
        protected string phone = "";
        protected string MSISDN = "";
        protected string slug = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            article_id = Common.GetValue("id", "0");

            slug = Request.QueryString["slug"] != null ? Common.ValidateXSS(Request.QueryString["slug"]) : "";

            string jsonData;
            
            try
            {
                jsonData = KPILogger.LogKPI("Article", () =>
                {
                    string cacheName = "TiinAPI:GetArticleFastReadSingle:" + article_id + "_" + slug;
                    var cachedJsonData = CacheManager.Remember<string>(cacheName,
                        DateTime.Now.AddSeconds(Common.SecondsCache),
                        delegate
                        {
                            var dataArt = new DataArticle(null);
                            var article = new MochaArticle();

                            DataSet ds = Common.GetArticleFastRead(article_id, slug);

                            if (ds.Tables[0].Rows.Count < 1 || !ds.Tables[0].Rows[0].HasColumn("Id") ||
                                string.IsNullOrEmpty(ds.Tables[0].Rows[0]["id"].ToString()) ||
                                ds.Tables[0].Rows[0]["id"].ToString() == "0")
                            {
                                throw new FileNotFoundException();
                            }
                            else
                            {
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    article.Title = row["Title"].ToString();
                                    article.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                                    article.Sapo = row["lead"].ToString();
                                    article.Pid = Convert.ToInt32(row["pid"].ToString());
                                    article.Cid = Convert.ToInt32(row["cid"].ToString());
                                    article.Blocks = new FastRead().GetContentBlocks(
                                            Common.GenLinkImageFull(row["Content"].ToString()),
                                            row["id"].ToString(), out var totalBlocks)
                                        .ConvertAll(MochaArticleBlock.Converter);

                                    article.TotalBlocks = totalBlocks;
                                    article.AuthorName =
                                        row["Author"].ToString() + " / " + row["SourceName"].ToString();
                                    article.SourceName = row["SourceName"].ToString();
                                    article.ParentCategory = row.StringValue("CategoryName");

                                    article.ParentCategoryAlias = row.StringValue("CategoryAlias");

                                    article.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                                        ? row.StringValue("ChildCategoryName")
                                        : row.StringValue("CategoryName");
                                    article.CategoryAlias = !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                                        ? row.StringValue("ChildCategoryAlias")
                                        : row.StringValue("CategoryAlias");

                                    article.Id = Convert.ToInt32(row["ID"].ToString());
                                    if (article.Id > 884864)
                                    {
                                        article.Image =
                                            ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar + row["LeadImage"].ToString();
                                    }
                                    else
                                    {
                                        article.Image =
                                            ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar + row["LeadImage"].ToString();
                                    }

                                    article.Like = 0;
                                    article.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                                    article.Position = 0;
                                    article.Reads = Convert.ToInt32(row["hit"].ToString());
                                    article.Type = 1;
                                    article.StarId = 0;
                                    article.Facebook = "";
                                    article.ThematicId = Convert.ToInt32(row.StringValue("thematic_id"));
                                    article.ThematicName = row.StringValue("Thematic");
                                    article.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                                    article.Url = Rewrite.GenRewriteUrlDetail(row);
                                }

                                dataArt.data = article;
                            }
                            return JsonConvert.SerializeObject(dataArt,
                                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Include});
                        });
                    return cachedJsonData;
                });
            }
            catch (Exception exception)
            {
                jsonData = JsonConvert.SerializeObject(new DataArticle(null) {exception = exception},
                    new JsonSerializerSettings {NullValueHandling = NullValueHandling.Include});
            }

            Response.JsonString(jsonData);
        }
    }
}