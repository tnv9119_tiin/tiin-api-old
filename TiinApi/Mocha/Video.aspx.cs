﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using tiin.mocha.api.TiinApi.Mocha.Models;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.Mocha
{
    public class Video : System.Web.UI.Page
    {
        protected string url = "";
        protected string phone = "";
        protected string MSISDN = "";
        protected bool isVip = false;
        public string Url { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            url = Request.QueryString["url"];
            isVip = Common.GetValue("isVip", "0") == "1";

            var dataArticle =
                KPILogger.LogKPI("Video", () =>
                {
                    var _dataArticle = new DataArticle(null);
                    if (string.IsNullOrEmpty(url))
                    {
                        _dataArticle.exception = new FileNotFoundException(url);
                    }
                    else
                    {
                        var video = new MochaVideo();


                        try
                        {
                            string videoId = null;
                            try
                            {
                                videoId = Regex.Match(url, @"video/\w+/[\w-]+/(?<videoid>\d+)\.html").Groups["videoid"]
                                    .Value;
                                DataSet ds = Common.MochaGetVideoDetail(videoId);

                                if (ds.Tables.Count > 0)
                                {
                                    foreach (DataRow row in ds.Tables[0].Rows)
                                    {
                                        video.Id = Convert.ToInt32(row["ID"].ToString());
                                        video.Pid = Convert.ToInt32(row["pid"].ToString());
                                        video.Cid = Convert.ToInt32(row["cid"].ToString());
                                        video.Title = row["Title"].ToString();
                                        video.Shapo = row["lead"].ToString();
                                        string MediaUrl = video.Id > 884864
                                            ? isVip ? ConfigurationManager.AppSettings["MediaRootVipNew"].TrimEnd('/') :
                                            ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/')
                                            : isVip
                                                ? ConfigurationManager.AppSettings["MediaRootVip"].TrimEnd('/')
                                                : ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/');

                                        video.Image = MediaUrl + Path.AltDirectorySeparatorChar +
                                                      row["LeadImage"].ToString();

                                        video.Url = Rewrite.GenRewriteUrlDetail(row);
                                        video.Media = MediaUrl +
                                                      Path.AltDirectorySeparatorChar + row["Media"].ToString();

                                        video.Poster = "";
                                        video.SourceName = row.Field<string>("SourceName");
                                        video.SourceIcon =
                                            ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            "/archive/flash/2019/06/27/flash012354292069758.png";
                                        video.DatePub = "<span style=\"color:#6D6D6D; font-size: 12px;\">" +
                                                        row.Field<string>("DatePub") + "</span>";
                                        video.View = row.Field<int>("Hit");
                                        video.UnixTime = row.Field<long>("UnixTime");
                                        video.Sid = row.Field<int>("Sid");

                                        /*
                                        var downloadAbleMedia =
                                            ConfigurationManager.AppSettings["MediaRootDownload"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar + row["Media"].ToString();

                                        var ffprobeReturn = CacheManager.Remember(downloadAbleMedia,
                                            DateTime.Now.AddMonths(1),
                                            delegate
                                            {
                                                return ConsoleAppWrapper.Execute(
                                                    Server.MapPath("~\\App_Data\\ffmpeg\\ffprobe.exe"),
                                                    " -hide_banner -show_format -show_streams -pretty " + "\"" +
                                                    downloadAbleMedia + "\"");
                                            });

                                        long duration;
                                        try
                                        {
                                            var widthStr = Regex.Match(ffprobeReturn, @"^width=(?<width>\d+)",
                                                RegexOptions.Multiline).Groups["width"].Value;
                                            var heightStr = Regex.Match(ffprobeReturn, @"^height=(?<height>\d+)",
                                                RegexOptions.Multiline).Groups["height"].Value;
                                            var durationStr = Regex.Match(ffprobeReturn,
                                                    @"^duration_ts=(?<duration_ts>\d+)", RegexOptions.Multiline)
                                                .Groups["duration_ts"].Value;

                                            height = int.Parse(heightStr.Trim());
                                            width = int.Parse(widthStr.Trim());
                                            duration = long.Parse(durationStr.Trim());
                                            video.duration = duration;
                                        }
                                        catch (Exception ex)
                                        {
                                            // Syntax error in the regular expression
                                        }
                                        */
                                        decimal ratio = (decimal) 1.78;
                                        decimal dbRatio = ratio;
                                        decimal.TryParse(row["Ratio"].ToString(), out dbRatio);
                                        if (dbRatio != 0)
                                        {
                                            ratio = dbRatio;
                                        }

                                        video.aspecRatio = ratio.ToString("0.000");
                                    }
                                }
                            }
                            catch (ArgumentException ex)
                            {
                                _dataArticle.exception = new FileNotFoundException(url);
                            }
                        }
                        catch (Exception ex)
                        {
                            _dataArticle.exception = ex;
                        }

                        _dataArticle.data = video;
                    }
                    return _dataArticle;
                });


            Response.Json(dataArticle);
        }
    }
}