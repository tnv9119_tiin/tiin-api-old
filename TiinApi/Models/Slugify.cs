using Newtonsoft.Json;

namespace ViettelMedia.TiinApi.Models
{
      public class Slugify
      {
            [JsonProperty("URL")]
            public string URL { get; set; }

            [JsonProperty("Type")]
            public int Type { get; set; }

            [JsonProperty("Title")]
            public string Title { get; set; }

            [JsonProperty("Slug")]
            public string Slug { get; set; }

            [JsonProperty("Id")]
            public string Id { get; set; }

            [JsonProperty("Category")]
            public string Category { get; set; }

            public Slugify()
            {
                  URL      = "";
                  Type     = 0;
                  Title    = "";
                  Slug     = "";
                  Id       = "";
                  Category = "";
            }
      }
}