﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace ViettelMedia.TiinApi.Models
{
      public class Article
      {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("cid")]
            public int Cid { get; set; }

            [JsonProperty("pid")]
            public int Pid { get; set; }

            [JsonProperty("url")]
            public string Url { get; set; }

            [JsonProperty("title")]
            public string Title { get; set; }

            [JsonProperty("image")]
            public string Image { get; set; }

            [JsonProperty("image_large")]
            public string ImageLarge { get; set; }

            [JsonProperty("content")]
            public string Content { get; set; }

            [JsonProperty("sapo")]
            public string Sapo { get; set; }

            [JsonProperty("datePub")]
            public int DatePub { get; set; }

            [JsonProperty("like")]
            public int Like { get; set; }

            [JsonProperty("comment")]
            public int Comment { get; set; }

            [JsonProperty("reads")]
            public int Reads { get; set; }

            [JsonProperty("type")]
            public int Type { get; set; }

            [JsonProperty("category")]
            public string Category { get; set; }

            [JsonProperty("category_alias")]
            public string CategoryAlias { get; set; }

            [JsonProperty("parent_category")]
            public string ParentCategory { get; set; }

            [JsonProperty("parent_category_alias")]
            public string ParentCategoryAlias { get; set; }

            [JsonProperty("fan_count")]
            public int FanCount { get; set; }

            [JsonProperty("star_id")]
            public int StarId { get; set; }

            [JsonProperty("name_star")]
            public string NameStar { get; set; }

            [JsonProperty("author_name")]
            public string AuthorName { get; set; }

            [JsonProperty("source_name")]
            public string SourceName { get; set; }

            [JsonProperty("video_id")]
            public int VideoId { get; set; }

            [JsonProperty("position")]
            public int Position { get; set; }

            [JsonProperty("loai_bai")]
            public int LoaiBai { get; set; }

            [JsonProperty("facebook")]
            public string Facebook { get; set; }


            [JsonProperty("media_url")]
            public string MediaUrl { get; set; }

            [JsonProperty("thematic_name")]
            public string ThematicName { get; set; }

            [JsonProperty("blocks")]
            public List<ArticleBlock> Blocks { get; set; }

            [JsonProperty("total_blocks")]
            public int TotalBlocks { get; set; }

            public Article()
            {
                  Id                  = 0;
                  Cid                 = 0;
                  Pid                 = 0;
                  Url                 = "";
                  Title               = "";
                  Image               = "";
                  ImageLarge          = "";
                  Content             = "";
                  Sapo                = "";
                  DatePub             = 0;
                  Like                = 0;
                  Comment             = 0;
                  Reads               = 0;
                  Type                = 0;
                  Category            = "";
                  CategoryAlias       = "";
                  ParentCategory      = "";
                  ParentCategoryAlias = "";
                  FanCount            = 0;
                  StarId              = 0;
                  NameStar            = "";
                  AuthorName          = "";
                  SourceName          = "";
                  VideoId             = 0;
                  Position            = 0;
                  LoaiBai             = 0;
                  Facebook            = "";
                  MediaUrl            = "";
                  ThematicName        = "";
                  Blocks              = new List<ArticleBlock>();
            }

            public void ReplaceImageCaption()
            {
                  Content = Regex.Replace(Content, "class=\"p-chuthich\" style=\"(.*?)\"",
                                          "class=\"p-chuthich\" style=\"background: #f5f5f5;font-size:1em; font-style: italic;color: #666;text-align: center;margin: -1em -0.5em 0;padding: 0.25em 0.5em;\"");
            }
      }
}