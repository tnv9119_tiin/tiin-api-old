﻿using Newtonsoft.Json;

namespace ViettelMedia.TiinApi.Models
{
      public class Category
      {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("image")]
            public string Image { get; set; }

        [JsonProperty("selected")]
        public bool Selected { get; set; }

        public Category()
            {
                  Id    = 0;
                  Name  = "";
                  Image = "";
            Selected = true;
            }
      }
}