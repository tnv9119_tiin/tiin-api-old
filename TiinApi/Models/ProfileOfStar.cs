﻿using Newtonsoft.Json;

namespace ViettelMedia.TiinApi.Models
{
      public class ProfileOfStar
      {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("fullname")]
            public string Fullname { get; set; }

            [JsonProperty("avatar_path")]
            public string AvatarPath { get; set; }

            [JsonProperty("week_fan_count")]
            public int WeekFanCount { get; set; }

            [JsonProperty("fan_count")]
            public int FanCount { get; set; }

            [JsonProperty("gender")]
            public int Gender { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("profile")]
            public string Profile { get; set; }

            [JsonProperty("is_saofan")]
            public int IsSaoFan { get; set; }

            public ProfileOfStar()
            {
                  Id           = 0;
                  Fullname     = "";
                  AvatarPath   = "";
                  WeekFanCount = 0;
                  FanCount     = 0;
                  Gender       = 0;
                  Address      = "";
                  Profile      = "";
                  IsSaoFan     = 0;
            }
      }
}