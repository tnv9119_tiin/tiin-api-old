﻿using System;
using Newtonsoft.Json;

namespace ViettelMedia.TiinApi.Models
{
      public class ListStar
      {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("cid")]
            public int Cid { get; set; }

            [JsonProperty("pid")]
            public int Pid { get; set; }

            [JsonProperty("url")]
            public string Url { get; set; }

            [JsonProperty("title")]
            public string Title { get; set; }

            [JsonProperty("image")]
            public string Image { get; set; }

            [JsonProperty("content")]
            public string Content { get; set; }


            [JsonProperty("sapo")]
            public string Sapo { get; set; }

            [JsonProperty("datePub")]
            public int DatePub { get; set; }

            [JsonProperty("like")]
            public int Like { get; set; }

            [JsonProperty("comment")]
            public int Comment { get; set; }

            [JsonProperty("reads")]
            public int Reads { get; set; }

            [JsonProperty("type")]
            public int Type { get; set; }

            [JsonProperty("fan_count")]
            public int FanCount { get; set; }

            [JsonProperty("star_id")]
            public int StarId { get; set; }

            [JsonProperty("video_id")]
            public int VideoId { get; set; }

            [JsonProperty("position")]
            public int Position { get; set; }

            [JsonProperty("loai_bai")]
            public int LoaiBai { get; set; }


            public ListStar()
            {
                  Id       = 0;
                  Cid      = 0;
                  Pid      = 0;
                  Url      = "";
                  Title    = "";
                  Image    = "";
                  DatePub  = 0;
                  Like     = 0;
                  Comment  = 0;
                  Reads    = 0;
                  Type     = 0;
                  FanCount = 0;
                  StarId   = 0;
                  VideoId  = 0;
                  Position = 0;
                  LoaiBai  = 0;
            }
      }
}