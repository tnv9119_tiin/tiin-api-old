﻿using System.Collections.Generic;
using TiinApi;

namespace ViettelMedia.TiinApi.Models.Response
{
      public class DataGetSetting : DataResponse
      {
            public DataGetSetting(List<Setting> data)
            {
                  this.data = data;
            }
      }
}