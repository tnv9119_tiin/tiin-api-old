//https://wiki.openjdk.java.net/display/Nashorn/Nashorn+extensions

// Using JavaImporter to resolve classes
// from specified java packages within the
// 'with' statement below
// more or less regular java code except for static types

function readText(url) {
    with (new JavaImporter(java.io, java.net)) {
        var is = new URL(url).openStream();
        try {
            var reader = new BufferedReader(
                new InputStreamReader(is));
            var buf = '', line = null;
            while ((line = reader.readLine()) != null) {
                buf += line;
            }
        } finally {
            reader.close();
        }
        return buf;
    }
}

var console_log = function(){
    for (var i = 0; i < arguments.length; i++) {
        if (typeof arguments[i] === "object"){
            print(JSON.stringify(arguments[i]));
        }else
            print(arguments[i]);
    }
};
var console = {
    log:console_log,
    warn:console_log,
    error:console_log,
};

load('https://cdnjs.cloudflare.com/ajax/libs/ajv/6.5.4/ajv.min.js');

//print(__DIR__.replace(/(\\)/g,'/'));
var schemaUrl = 'file:///'+__DIR__.split("\\").join("/")+schema+'.json';
//print(schemaUrl);

client.test("Validate JSON Schema",function () {
    if (typeof schema === 'string'){
        schema = JSON.parse(readText(schemaUrl));
    }

    var ajv = new Ajv({allErrors: true});
    var valid = ajv.validate(schema,response.body);
    console.log(ajv.errors);
    client.assert(valid,"Json schema correct")
});
